import axios, { AxiosResponse, InternalAxiosRequestConfig, AxiosError } from 'axios'
import { ResultEnum } from "@/enums/httpEnum"
import { ErrorPageNameMap } from "@/enums/pageEnum"
import { redirectErrorPage, getCookie } from '@/utils'

const axiosInstance = axios.create({
  baseURL: import.meta.env.VITE_APP_BASE_API ,
  timeout: ResultEnum.TIMEOUT,
})

axiosInstance.interceptors.request.use(
  (config: InternalAxiosRequestConfig) => {

    const token = getCookie('authorized-token');
    const projectId = getCookie('projectId');
    if (token) {
      config.headers["Authorization"] = "Bearer " + token;
      config.headers["project"] = projectId;
      return config;
    } else {
      return config;
    }
  },
  (error: AxiosError) => {
    Promise.reject(error)
  }
)

// 响应拦截器
axiosInstance.interceptors.response.use(
  (res: AxiosResponse) => {
    const { code } = res.data as { code: number }
    if (code === undefined || code === null) return Promise.resolve(res.data)
    if (code === ResultEnum.DATA_SUCCESS) return Promise.resolve(res.data)
    // 重定向
    if (ErrorPageNameMap.get(code)) redirectErrorPage(code)
    return Promise.resolve(res.data)
  },
  (err: any) => {
    if (err.response && err.response.status === 403) {
      // window.location.href = '/login'
      window.location.href = window.location.origin + window.location.pathname + '#/login'
      return Promise.reject(err)
    }
    return Promise.reject(err)
  }
)

export default axiosInstance
