import axiosInstance from '../axios';
import {
  RequestHttpEnum,
  ContentTypeEnum,
  RequestBodyEnum,
  RequestDataTypeEnum,
  RequestContentTypeEnum,
  RequestParamsObjType
} from '@/enums/httpEnum';

// 用户登录
export const login = (data: Object) => {
  return axiosInstance({
    url: "/auth/login",
    method: RequestHttpEnum.POST,
    data: data,
    headers: {
      'Content-Type': ContentTypeEnum.JSON
    }
  });
};

// // 刷新token(目前不存在刷新token)
// export function refreshToken(params) {
//   return http.request<RootObject<LoginModel>>({
//     url: http.authUrl("/auth/refresh"),
//     method: "post",
//     params
//   });
// }

// // 登出
export function logout(params: any) {
  return axiosInstance({
    url: "/auth/loginOut",
    method: RequestHttpEnum.GET,
    params
  });
}
