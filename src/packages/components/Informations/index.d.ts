export enum ChatCategoryEnum {
  TEXT = 'Texts',
  TITLE = 'Titles',
  INPUTS = 'Inputs',
  BUTTONS = 'Buttons',
  MORE = 'Mores'
}

export enum ChatCategoryEnumName {
  TEXT = '文本',
  TITLE = '标题',
  // 控件 => 数据录入
  INPUTS = '控件',
  BUTTONS = '按钮',
  MORE = '更多'
}