import { InteractEventOn, InteractActionsType } from '@/enums/eventEnum'

// 组件类型
export enum ComponentInteractEventEnum {
    CLICK = 'click'
}

// 联动参数
export enum ComponentInteractParamsEnum {
    DATA = 'data'
}

// 定义组件触发回调事件
export const interactActions: InteractActionsType[] = [
    {
        interactType: InteractEventOn.CLICK,
        interactName: '点击事件',
        componentEmitEvents: {
            [ComponentInteractEventEnum.CLICK]: [
                {
                    value: ComponentInteractParamsEnum.DATA,
                    label: '点击数据'
                }
            ]
        }
    }
]