import { ConfigType, PackagesCategoryEnum, ChartFrameEnum } from '@/packages/index.d'
import { ChatCategoryEnum, ChatCategoryEnumName } from '../../index.d'

export const ButtonCommonConfig: ConfigType = {
  key: 'ButtonCommon',
  chartKey: 'VButtonCommon',
  conKey: 'VCButtonCommon',
  title: '按钮',
  category: ChatCategoryEnum.BUTTONS,
  categoryName: ChatCategoryEnumName.BUTTONS,
  package: PackagesCategoryEnum.INFORMATIONS,
  chartFrame: ChartFrameEnum.COMMON,
  image: 'button_common.png'
}