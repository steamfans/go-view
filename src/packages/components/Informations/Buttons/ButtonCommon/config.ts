import cloneDeep from 'lodash/cloneDeep'
import { PublicConfigClass } from '@/packages/public'
import { CreateComponentType } from '@/packages/index.d'
import { chartInitConfig } from '@/settings/designSetting'
import { COMPONENT_INTERACT_EVENT_KET } from '@/enums/eventEnum'
import { interactActions, ComponentInteractEventEnum } from './interact'
import { ButtonCommonConfig } from './index'

export const option = {
    // 组件展示类型，必须和 interactActions 中定义的数据一致
    [COMPONENT_INTERACT_EVENT_KET]: ComponentInteractEventEnum.CLICK,
    // 
    link: '',
    // 按钮文本
    text: '按钮',
    // 按钮类型
    type: 'primary',
    // 按钮尺寸
    size: 'medium',
    // 是否禁用
    disabled: false,
    // 是否显示加载中
    loading: false
}

export default class Config extends PublicConfigClass implements CreateComponentType {
    public key = ButtonCommonConfig.key
    public attr = { ...chartInitConfig, w: 260, h: 32, zIndex: -1 }
    public chartConfig = cloneDeep(ButtonCommonConfig)
    public interactActions = interactActions
    public option = cloneDeep(option)
}