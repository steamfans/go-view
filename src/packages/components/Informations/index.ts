import Texts from './Texts'
import Inputs from './Inputs'
import Mores from './Mores'
import Buttons from './Buttons'

export const InformationList = [...Texts, ...Inputs, ...Buttons, ...Mores, ]
